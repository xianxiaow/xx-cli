extern crate xx_cli;

use xx_cli::config::Config;

fn main() {
  let config = Config::new();
  println!("command {}", config.command );
}
