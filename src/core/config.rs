use argparse::{ArgumentParser, Store, StoreTrue};

pub struct Config {
  pub verbose: bool,
  pub command: String,
}

impl Config{
  pub fn new() -> Config {
    let mut verbose = false;
    let mut command = "".to_string();
    {
      let mut ap = ArgumentParser::new();
      ap.set_description("欢迎使用xx-cli工具");
      ap.refer(&mut command)
        .add_argument("command", Store, "模式，目前仅支持init");
      ap.refer(&mut verbose)
        .add_option(&["-v", "--verbose"], StoreTrue, "Be verbose");
      ap.parse_args_or_exit();
    }
    Config{ verbose, command }
  }
}
